<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";
$objCity = new \App\City\City();
$objCity->setData($_GET);
$oneData = $objCity->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City Add Form</title>
</head>
<body>
<h2 style="text-align:center ">City Add Form</h2>

<form action="update.php" method="post">

    <table border="1"; width=500 align=center bgcolor="#e6e6fa">

        <tr><td> select Your City Name:</td>
            <td><select name="city" value="<?php echo $oneData->city?>">
                    <option >Phnom Penh</option>
                    <option >Kampong Soam</option>
                    <option >Siem Reap</option>
                    <option >Dhaka</option>
                </select></td></tr>
        <input type="hidden" name="id" value="<?php echo $oneData->id?>" >


        <tr><td colspan="2"> <input type="submit" value="update"></td></tr>

    </table >
</form>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>